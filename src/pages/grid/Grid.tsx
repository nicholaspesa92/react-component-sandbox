import * as React from 'react';
import styled from 'styled-components';

const _Container = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: 100%;
`;

const _Item = styled.div`
    flex: 1 1 100%;
    height: 200px;
    min-width: 100px;
    max-width: 300px;

    display: flex;
    align-items: center;
    justify-content: center;
    border: 1px dashed rgba(0,0,0,0.14);
`;

const Grid = () => {
    const items = new Array(100).fill(undefined);

    return (
        <_Container>
            {
                items.map((item, i) => {
                    return (
                        <_Item>{i}</_Item>
                    );
                })
            }
        </_Container>
    )
}

export { Grid }; 
