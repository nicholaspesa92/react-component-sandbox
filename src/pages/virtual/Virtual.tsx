import * as React from 'react';
import styled from 'styled-components';
import { VirtualDataGrid, VirtualMode } from '../../components/virtual';
import { GestureBlock } from './../../components/gesture';

const _Container = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    
    > * {
        margin-bottom: 48px;
    }
`;

const _Item = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    height: 2000px;
    width: 100%;
    border: 1px dashed rgba(0,0,0,0.24);
    box-sizing: border-box;

    .scroller {
        width: 3000px;
        height: 200px;
        background: linear-gradient(90deg, red, yellow);
        margin-top: 24px;
    }
`;

const _SmolItem = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    height: 100px;
    width: 100%;
    border: 1px dashed rgba(0,0,0,0.24);
    box-sizing: border-box;

    .scroller {
        width: 3000px;
        height: 200px;
        background: linear-gradient(90deg, red, yellow);
        margin-top: 24px;
    }
`;

const bigItemWidth = 670;

const Item = (i: number, j: number, itemWidth: number) => {
    return (
        <_Item>
            <div>{i}: {j}</div>
            <GestureBlock>
                <div style={{ width: '100%', overflow: 'auto' }}>
                    <div className={'scroller'} />
                </div>
            </GestureBlock>
        </_Item>
    );
}

const getTabIndex = (h: number, t: number) => (parseInt(`${h}${(t + 1).toString().length > 1 ? (t + 1) : `0${t + 1}`}`, 10));

const SmallItem = (row: number, col: number, itemWidth: number) => {
    return (
        <_SmolItem><div tabIndex={getTabIndex(col, row)}>{row}: {col}</div></_SmolItem>
    )
}

const headerItemRenderer = () => {
    return <div />
}

const Virtual = () => {
    return (
        <_Container>
            <VirtualDataGrid
                colCount={100}
                rowCount={100}
                itemWidth={300}
                itemHeight={100}
                gutter={8}
                buffer={1}
                view={0}
                centerItems={false}
                mode={VirtualMode.WINDOWED}
                cellRenderer={SmallItem}
                headerItemRenderer={headerItemRenderer}
                headerHeight={0}
            />
        </_Container>
    );
};

export { Virtual };
