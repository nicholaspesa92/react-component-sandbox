import * as React from 'react';

interface IGestureBlockProps {
    children?: JSX.Element | JSX.Element[];
}

const GestureBlock = ({children}: IGestureBlockProps) => {

    const ref = React.useRef<HTMLDivElement>(null);

    React.useEffect(() => {
        if (ref.current) { ref.current.addEventListener('touchmove', stopEvent, {passive: false}); }
        if (ref.current) { ref.current.addEventListener('wheel', stopEvent, {passive: false}); }
    }, []);

    const stopEvent = (e: TouchEvent | WheelEvent) => {
        e.stopPropagation();
        console.log(e);
    }

    return (
        <div ref={ref} style={{ width: '100%' }}>{children}</div>
    )
}

export { GestureBlock };
