import * as React from 'react';
import { useGesture } from 'react-use-gesture';
import { Coordinates, FullGestureState } from 'react-use-gesture/dist/types';

interface IGestureWrapperProps {
    children?: JSX.Element;
    onDrag?: (state: FullGestureState<Coordinates>) => void;
}

const GestureWrapper = ({
    children,
    onDrag,
}: IGestureWrapperProps) => {

    const bindGestures = useGesture({
        onDrag: (state: FullGestureState<Coordinates>) => {
            if (onDrag) { onDrag(state); }
        },
    });

    return (
        <div {...bindGestures()} style={{width: '100%'}}>
            {children}
        </div>
    );
};

export { GestureWrapper };
