import * as React from 'react';
import './Hamburger.scss';

interface IHamburgerProps {
    isOpen: boolean;
    onClick: () => void;
}

const Hamburger  = ({isOpen, onClick}: IHamburgerProps) => {
    return (
        <div className={`hamburger ${isOpen ? 'open' : 'closed'}`} onClick={onClick}>
            <div className={'row top'} />
            <div className={'row middle'} />
            <div className={'row bottom'} />
        </div>
    )
}

export { Hamburger };
