import * as React from 'react';
import styled from 'styled-components';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { Coordinates, FullGestureState } from 'react-use-gesture/dist/types';

const _Wrapper = styled('div')<{gutter: number}>`
    display: flex;
    box-sizing: border-box;
    &.fullscreen {
        height: 100vh;
        width: 100vw;
        position: fixed;
        top: 0;
        left: 0;
        background-color: rgba(0,0,0,0.14);
        z-index: 100;
    }
    &.windowed {
        width: 100%;
    }

    &.open {
        visibility: visible;
    }
`;

interface IBg {
    cellWidth: number;
    colCount: number;
    gutter: number;
    centerItems: boolean;
}

const _Bg = styled('div')<IBg>`
    position: fixed;
    height: 100vh;
    width: 100vw;
    background-color: rgba(0,0,0,0.7);
    top: 0;
    left: 0;
    padding-left: ${({cellWidth, centerItems}) => centerItems ? `calc(50vw - (${cellWidth}px / 2))` : '0'};
    min-width: ${({cellWidth, colCount, gutter, centerItems}) => {
        return centerItems
            ? `calc(${cellWidth * colCount}px + ${gutter * (colCount - 1)}px + (50vw - ${cellWidth}px / 2))`
            : `calc(${cellWidth * colCount}px + ${gutter * (colCount - 1)}px)`;
        }
    };
`;

interface IVTScrollContainer {
    itemWidth: number;
    itemHeight: number;
    rowCount: number;
    gutter: number;
    centerItems: boolean;
    headerHeight: number;
    auto: boolean;
}

const _ScrollContainer = styled('div')<IVTScrollContainer>`
    display: flex;
    flex-direction: column;
    width: 100%;
    max-width: 100%;
    min-width: 100%;
    height: ${({itemHeight, rowCount, gutter, headerHeight, auto}) => `${auto ? `100%` : `${(rowCount * ((itemHeight) + gutter)) + headerHeight}px`}`};
    overflow-x: hidden;
    padding-left: ${({itemWidth, centerItems}) => centerItems ? `calc(50vw - (${itemWidth}px / 2))` : '0'};
    position: relative;
    transform: translateZ(0);
    -webkit-transform: translateZ(0);
    box-sizing: border-box;

    &.touch {
        overflow-x: auto;
    }

    &::-webkit-scrollbar {
        width: 0px;  /* Remove scrollbar space */
        height: 0px;
        background: transparent;  /* Optional: just make scrollbar invisible */
    }
`;

interface IVTScrollRow {
    itemWidth: number;
    itemHeight: number;
    colCount: number;
    rowCount: number;
    gutter: number;
    centerItems: boolean;
}

const _FillRow = styled('div')<IVTScrollRow>`
    position: relative;
    display: flex;
    flex-direction: row;
    transition: 100ms ease;
    min-height: ${({itemHeight}) => `${itemHeight}px`};
    min-width: ${({itemWidth, colCount, gutter, centerItems}) => {
        return centerItems
            ? `calc(${itemWidth * colCount}px + ${gutter * (colCount - 1)}px + (50vw - ${itemWidth}px / 2))`
            : `calc(${itemWidth * colCount}px + ${gutter * (colCount - 1)}px)`;
        }
    };

    &.header {
        position: sticky;
        top: 0;
    }
`;

interface IVTHeaderItem {
    itemWidth: number;
}

const _HeaderItem = styled('div')<IVTHeaderItem>`
    display: flex;
    justify-content: center;
    box-sizing: border-box;
    width: ${({itemWidth}) => `${itemWidth}px`};
`;

interface IVTItem {
    itemHeight: number;
    itemWidth: number;
    gutter: number;
    left: number;
    auto: boolean;
}

const _Item = styled('div')<IVTItem>`
    position: absolute;
    display: flex;
    justify-content: center;
    box-sizing: border-box;
    max-height: ${({gutter}) => `calc(100vh - (${gutter / 2}px))`};
    overflow: auto;
    height: ${({itemHeight, gutter, auto}) => `${auto ? 'auto' : `${itemHeight + gutter}px`}`};
    width: ${({itemWidth}) => `${itemWidth}px`};
    left: ${({left}) => `${left}px`};
    margin-top: ${({gutter}) => `${gutter / 2}px`};

    &.peeking { opacity: 0.8; }

    scrollbar-width: none;
    &::-webkit-scrollbar { display: none; }
`;

interface IVTArrow {
    width: number;
    cellWidth: number;
    gutter: number;
}

const _Arrow = styled('div')<IVTArrow>`
    display: flex;
    align-items: center;
    justify-content: center;
    position: absolute;
    top: 200px;
    width: ${({width}) => `${width}px`};
    height: 34px;
    cursor: pointer;

    img {
        height: 100%;
        width: 100%;
    }
`;

export enum VirtualMode {
    FULLSCREEN = 'FULLSCREEN',
    WINDOWED = 'WINDOWED',
}

export enum HorizontalScrollDireciton {
    LEFT = 'LEFT',
    RIGHT = 'RIGHT',
}

export interface IVirtualDataGridProps {
    isOpen?: boolean;
    colCount: number;
    rowCount: number;
    itemWidth: number;
    itemHeight: number;
    view: number;
    tableId?: string;
    headerHeight: number;
    gutter?: number;
    buffer?: number;
    centerItems?: boolean;
    mode?: VirtualMode;
    spread?: boolean;
    autoHeight?: boolean;
    cellRenderer: (row: number, col: number, itemWidth: number) => JSX.Element;
    headerItemRenderer: (col: number, tabIndex: number) => JSX.Element;
    onCenterChange?: (center: number) => void;
    onClose?: () => void;
}

export interface ListRange {
    low: number;
    high: number;
}

const VirtualDataGrid = ({
    colCount,
    rowCount,
    itemWidth,
    itemHeight,
    view,
    headerHeight,
    gutter = 0,
    buffer = 1,
    centerItems = false,
    mode = VirtualMode.WINDOWED,
    autoHeight = false,
    cellRenderer,
    headerItemRenderer,
    onCenterChange,
    onClose,
}: IVirtualDataGridProps) => {
    const resistance = 1;
    const bounceTime = 50;
    const scrollContainerRef: React.Ref<HTMLDivElement> = React.useRef(null);

    const [headerItems, setHeaderItems] = React.useState(new Array(colCount).fill(undefined));
    const [gridItems, setGridItems] = React.useState(new Array(rowCount).fill(undefined).map((_) =>  new Array(colCount).fill(undefined)));

    const [range, setRange] = React.useState({low: 0, high: 1});
    const [center, setCenter] = React.useState(view);

    const [rangeSubject] = React.useState(new Subject<ListRange>());

    const [cellWidth, setCellWidth] = React.useState(itemWidth);

    let counter1 = 0;
    let counter2: number;
    let marker = true;

    React.useEffect(() => {
        const sub = rangeSubject.pipe(debounceTime(bounceTime)).subscribe((_range: ListRange) => { setRange(_range); });
        init();
        return (() => {
            if (sub) sub.unsubscribe();
            setRange({low: 0, high: 0});
        });
    }, []);

    React.useEffect(() =>  gotoIndex(center), [center]);

    React.useEffect(() => {
        setHeaderItems(new Array(colCount).fill(undefined));
        setGridItems(new Array(rowCount).fill(undefined).map((_) =>  new Array(colCount).fill(undefined)));
    }, [colCount, rowCount]);

    const init = () => {
        if (scrollContainerRef.current) {
            const enoughItemsToFill = ((itemWidth + gutter) * colCount) < scrollContainerRef.current.clientWidth;
            if (!centerItems && enoughItemsToFill) {
                setCellWidth((scrollContainerRef.current.clientWidth / colCount) - gutter);
            }
            wheelInit();
            calculateCells();
            scrollContainerRef.current.scrollLeft = leftCalc(view);
            if (onCenterChange) { onCenterChange(view); }
        }
    };

    const scroll = (e: React.UIEvent<HTMLDivElement>) => calculateCells();

    const calculateCells = () => {
        if (scrollContainerRef.current) {
            const width = scrollContainerRef.current.clientWidth;
            const left = scrollContainerRef.current.scrollLeft;
            const first = ((left / (cellWidth + gutter)) - 1) >= 0 ? (Math.floor(left / (cellWidth + gutter)) - 1) : 0;
            const last = Math.floor(first + (width / (cellWidth + gutter)));
            if (hasRangeChanged(first - buffer, last + buffer)) rangeSubject.next({low: first - buffer, high: last + buffer});
            if (centerItems) {
                if (left === leftCalc(center) && onCenterChange) onCenterChange(center);
            } else {
                if (onCenterChange) onCenterChange(center);
            }
        }
    };

    const hasRangeChanged = (first: number, last: number) => {
        return range.low !== first || range.high !== last;
    };

    const wheelCallback = (e: React.WheelEvent<HTMLDivElement>) => {
        if (Math.abs(e.deltaY) < Math.abs(e.deltaX)) {
            if (scrollContainerRef.current && !centerItems) {
                scrollContainerRef.current.scrollLeft += (e.deltaX / resistance);
                counter1 += 1;
                if (marker) wheelStart();
            }
        }
        e.stopPropagation();
    };

    const wheelInit = () => {
        if (scrollContainerRef.current) {
            scrollContainerRef.current.addEventListener('wheel', (e: WheelEvent) => {
                if (Math.abs(e.deltaY) < Math.abs(e.deltaX) && centerItems) {
                    e.preventDefault();
                }
            }, { passive: false });
        }
    };

    const wheelStart = () => {
        marker = false;
        wheelAct();
    };

    const wheelAct = () => {
        counter2 = counter1;
        setTimeout(() => counter2 === counter1 ? wheelEnd() : wheelAct(), 50);
    };

    const wheelEnd = () => {
        marker = true;
        counter1 = 0;
        counter2 = 0;
        if (scrollContainerRef.current) snapToFirst();
    };

    const leftCalc = (index: number) => (index) * (cellWidth + gutter);

    const gotoIndex = (index: number, behavior: 'smooth' | 'auto' | undefined = 'smooth') => {
        if (scrollContainerRef.current) {
            const left = leftCalc(index);
            scrollContainerRef.current.scrollTo({ left, behavior });
        }
    };

    const snapToFirst = () => {
        if (scrollContainerRef.current) {
            const first = Math.round(scrollContainerRef.current.scrollLeft / (cellWidth + gutter));
            gotoIndex(first);
        }
    };

    const itemClick = (i: number) => {
        if (centerItems) {
            if (i < center) prev();
            if (i > center) next();
        }
    };

    const next = (e?: React.MouseEvent<HTMLDivElement> | React.TouchEvent<HTMLDivElement>) => {
        setCenter(center + 1);
    };

    const prev = (e?: React.MouseEvent<HTMLDivElement> | React.TouchEvent<HTMLDivElement>) => {
        setCenter(center - 1);
    };

    const close = (e?: React.MouseEvent<HTMLDivElement> | React.TouchEvent<HTMLDivElement>) => {
        if (onClose) onClose();
    };

    const arrows = () => {
        return (
            <>
                {
                    center > 0 &&
                    <_Arrow
                        width={54}
                        cellWidth={cellWidth}
                        gutter={gutter}
                        style={{left: `calc(50vw - (${(cellWidth / 2) + gutter}px))`}}
                        onClick={prev}
                    >
                        <img src={`{env.SVG_IMAGES}/arrow-left.svg`} />
                    </_Arrow>
                }
                {
                    (center < (colCount - 1)) &&
                    <_Arrow
                        width={54}
                        cellWidth={cellWidth}
                        gutter={gutter}
                        style={{right: `calc(50vw - (${(cellWidth / 2) + gutter}px))`}}
                        onClick={next}
                    >
                        <img src={`{env.SVG_IMAGES}/arrow-right.svg`} />
                    </_Arrow>
                }
            </>
        );
    };

    return (
        <_Wrapper className={mode === VirtualMode.FULLSCREEN ? 'fullscreen' : 'windowed'} onWheel={wheelCallback} gutter={gutter}>
            <_ScrollContainer
                ref={scrollContainerRef}
                itemWidth={cellWidth}
                itemHeight={itemHeight}
                rowCount={rowCount}
                gutter={gutter}
                centerItems={centerItems}
                headerHeight={headerHeight}
                id={'vt-scroll-container'}
                onScroll={scroll}
                auto={rowCount === 1}
            >
                <_FillRow
                    itemWidth={cellWidth}
                    itemHeight={headerHeight}
                    colCount={colCount}
                    rowCount={1}
                    gutter={gutter}
                    centerItems={centerItems}
                    className={'header'}
                >
                    {
                        headerItems.map((_, r) => {
                            return (
                                <_HeaderItem key={`header_${r}`} itemWidth={cellWidth}>
                                    {headerItemRenderer(r, 0)}
                                </_HeaderItem>
                            );
                        })
                    }
                </_FillRow>
                {
                    gridItems.map((rowItems, r) => {
                        return (
                            <_FillRow
                                key={r}
                                itemWidth={cellWidth}
                                itemHeight={itemHeight}
                                colCount={colCount}
                                rowCount={rowCount}
                                gutter={gutter}
                                centerItems={centerItems}
                            >
                                {mode === VirtualMode.FULLSCREEN && <_Bg cellWidth={cellWidth} colCount={colCount} gutter={gutter} centerItems={centerItems} onClick={close}/>}
                                {
                                    rowItems.map((_, c) => {
                                        const left = ((gutter + cellWidth) * c);
                                        return (
                                            ((c >= range.low && c <= range.high)) &&
                                            <_Item
                                                key={`${r}0${c}`}
                                                id={'scroll-item'}
                                                className={c === center ? 'centered' : 'peeking'}
                                                itemHeight={itemHeight}
                                                itemWidth={cellWidth}
                                                gutter={gutter}
                                                left={left}
                                                auto={autoHeight}
                                                onClick={() => itemClick(c)}
                                            >
                                                {cellRenderer(r, c, cellWidth)}
                                            </_Item>
                                        );
                                    })
                                }
                            </_FillRow>
                        );
                    })
                }
            </_ScrollContainer>
            {centerItems && arrows()}
        </_Wrapper>
    );
};

export { VirtualDataGrid };
