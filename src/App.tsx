import * as React from 'react';
import './App.scss';
import { AppLinks, AppRoutes } from './routes/AppRoutes';
import { BrowserRouter as Router } from 'react-router-dom';
import { Hamburger } from './components/ui/hamburger';


const App: React.FC = () => {

    const [isOpen, toggleDrawer] = React.useState(false);

    const toggle = () => {
        toggleDrawer(!isOpen);
    }

    return (
        <div className={'app-container'}>
            <Router>
                <div className={`drawer ${isOpen ? 'open' : 'closed'}`}>
                    <div className={'header'} />
                    <AppLinks />
                </div>
                <div className={'toolbar'}>
                    <Hamburger isOpen={isOpen} onClick={toggle}/>
                </div>
                <div className={'content'}>
                    <AppRoutes />
                </div>
            </Router>
        </div>
    );
}

export default App;
