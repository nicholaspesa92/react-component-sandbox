import * as React from 'react';
import { Route, Redirect, Link } from 'react-router-dom';
import Routes, { IRoute } from './Routes';

const AppLinks = () => {
    return (
        <div className={'nav-links'}>
        {
            Routes.map((route: IRoute) => {
                return (
                    <div className={'link'} key={`link_${route.path}`}>
                        <i className={'material-icons'}>{route.icon}</i>
                        <Link to={route.path}>{route.name}</Link>
                    </div>
                );
            })
        }
        </div>
    );
};

const AppRoutes = () => {
    return (
        <div style={{maxWidth: '100%', width: '100%'}}>
            <Route path={'/'} render={() => <Redirect to={'virtual'} />} />
            {
                Routes.map((route: IRoute) => {
                    return (
                        <Route key={route.path} path={route.path} component={route.component} />
                    );
                })
            }
        </div>
    );
};

export { AppLinks, AppRoutes };
