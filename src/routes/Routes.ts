import { Home, Virtual, Grid } from './../pages';

export interface IRoute {
    path: string;
    component: () => JSX.Element | JSX.Element;
    name: string;
    icon: string;
}

const Routes: IRoute[] = [
    {
        path: '/home',
        component: Home,
        name: 'Home',
        icon: 'home',
    },
    {
        path: '/virtual',
        component: Virtual,
        name: 'Virtual Components',
        icon: 'view_week',
    },
    {
        path: '/grid',
        component: Grid,
        name: 'Grid Component',
        icon: 'home'
    }
];

export default Routes;
